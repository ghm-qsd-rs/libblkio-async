use std::cell::{Cell, RefCell};
use std::collections::VecDeque;
use std::future::Future;
use std::io::{self, IoSlice, IoSliceMut};
use std::os::unix::io::RawFd;
use std::pin::Pin;
use std::rc::Rc;
use std::task::{Context, Poll, Waker};
use tokio::io::unix::AsyncFd;

#[cfg(feature = "x-blkio-test")]
use blkio_test as blkio;

/**
 * Encapsulates the blkio queue and other global state.
 */
pub struct Queue {
    blkioq: RefCell<&'static mut blkio::Blkioq>,

    /// Only one request can wait on the completion FD at a time, and that request will steal it
    /// from here
    completion_fd: Cell<Option<AsyncFd<RawFd>>>,

    /// List of all follower requests, sorted from oldest to newest.
    /// When a follower request is done, it does not remove itself from the list.  Doing so would
    /// be costly (O(n)), and is also unnecessary.  Finished requests are easily recognizable based
    /// on their `ret` value.
    /// When the leader is done, however, it will drain the queue until it finds a new currently
    /// pending request to designate as the new leader (which is then woken).
    followers: RefCell<VecDeque<Rc<RequestState>>>,

    /// Whether there currently is no leader running (i.e. no request at all), which requires that
    /// when a new request is created, it must be designated the leader
    leaderless: Cell<bool>,
}

/**
 * Represents a blkio queue with an async interface.  Does not implement `Send` or `Sync` and so
 * can only be used by a single thread at a time, and must be used by that thread.
 */
pub struct AsyncBlkioq {
    queue: Rc<Queue>,
}

/**
 * Core state of an ongoing request.  This object is separate from `Request` so that it can be put
 * in an `Rc` and be referenced from multiple sources, namely:
 * - `Request.rs`
 * - The user data field of requests submitted to libblkio
 * - The CFD waiters list
 *
 * Note: Putting `RequestState` into an `Rc` incurs a heap allocation and deallocation for every
 * request.  Merging `RequestState` into `Request` in order to prevent that however has only led to
 * a degredation in performance in practice (issue #1), presumably because the `Request` object is
 * moved in memory before being polled, and so an increase in size of the `Request` object is worse
 * than doing one allocation/deallocation.
 */
struct RequestState {
    /// Positive: In progress
    /// Zero: Completed successfully
    /// Negative: Failed
    ret: Cell<i32>,

    /// True iff this is the first request in `queue.requests`.  This is the request that will be
    /// waiting on the completion FD (only a single request can do that at a time), and the only
    /// one that will actually submit I/O (there is no point in letting multiple requests do so).
    leader: Cell<bool>,

    waker: Cell<Option<Waker>>,

    queue: Rc<Queue>,

    /// In order to wait on the completion FD, a request will steal `Queue.completion_fd`, create
    /// an `AsyncFd` from it, and put it here.  Once woken, the `AsyncFd` is unwrapped to get the
    /// original `RawFd`, which is then put back into `Queue.completion_fd`.
    completion_fd_waiter: Cell<Option<AsyncFd<i32>>>,
}

/**
 * Represents an async I/O request, and so implements the `Future` trait.  Like `AsyncBlkioq`, this
 * struct does not implement `Send` or `Sync`.
 */
pub struct Request<'a> {
    /// The actual request state.  From benchmarking, it appears that putting as much data as
    /// possible there is best for performance: We need a heap allocation for it anyway, so its
    /// size does not matter.  In contrast, it appears that Rust moves `Future` objects after their
    /// creation before the first poll (by which point they must be pinned), and so the bigger the
    /// object is, the more data has to be moved.
    /// (If all of `RequestState` were to be merged into `Request`, it would be so much data that
    /// copying/moving it takes more time than the heap allocation/deallocation (see also the note
    /// above `RequestState`).)
    rs: Rc<RequestState>,

    _lifetime: std::marker::PhantomData<&'a mut [u8]>,
}

impl AsyncBlkioq {
    /**
     * Create an `AsyncBlkioq` from a blkio queue reference.  The caller must ensure that the blkio
     * device lives at least as long as the returned `AsyncBlkioq` object.
     */
    pub fn new(blkioq: &'static mut blkio::Blkioq) -> Self {
        AsyncBlkioq {
            queue: Rc::new(Queue::new(blkioq)),
        }
    }

    /**
     * Create a new request object, and return it and a strong reference (pointer) to the
     * associated `RequestState`, which can be turned into an integer and used as a blkio request's
     * user data.
     */
    fn new_request<'a>(&self) -> (Request<'a>, *const RequestState) {
        let leader = self.queue.leaderless.take();
        let rs = Rc::new(RequestState::new(&self.queue, leader));
        let rs_ptr = Rc::into_raw(Rc::clone(&rs));

        if !leader {
            self.queue.followers.borrow_mut().push_back(Rc::clone(&rs));
        }

        let r = Request {
            rs,
            _lifetime: std::marker::PhantomData,
        };

        (r, rs_ptr)
    }

    /**
     * Issue a read request.
     */
    pub fn read<'a>(&self, start: u64, buf: &'a mut [u8], flags: blkio::ReqFlags) -> Request<'a> {
        let (r, rs_ptr) = self.new_request();

        self.queue.blkioq.borrow_mut().read(
            start,
            buf.as_mut_ptr(),
            buf.len(),
            rs_ptr as usize,
            flags,
        );

        r
    }

    /**
     * Issue a read request, passing a vector of buffers.
     */
    pub fn readv<'a>(
        &self,
        start: u64,
        buf_vec: &'a mut [IoSliceMut<'a>],
        flags: blkio::ReqFlags,
    ) -> Request<'a> {
        let (r, rs_ptr) = self.new_request();

        // IoSliceMut and libc::iovec are guaranteed to have the same representation
        let iovec = unsafe {
            std::mem::transmute::<*const IoSliceMut, *const libc::iovec>(buf_vec.as_ptr())
        };

        self.queue.blkioq.borrow_mut().readv(
            start,
            iovec,
            buf_vec.len() as u32,
            rs_ptr as usize,
            flags,
        );

        r
    }

    /**
     * Issue a write request.
     */
    pub fn write<'a>(&self, start: u64, buf: &'a [u8], flags: blkio::ReqFlags) -> Request<'a> {
        let (r, rs_ptr) = self.new_request();

        self.queue.blkioq.borrow_mut().write(
            start,
            buf.as_ptr(),
            buf.len(),
            rs_ptr as usize,
            flags,
        );

        r
    }

    /**
     * Issue a write request, passing a vector of buffers.
     */
    pub fn writev<'a>(
        &self,
        start: u64,
        buf_vec: &'a [IoSlice<'a>],
        flags: blkio::ReqFlags,
    ) -> Request<'a> {
        let (r, rs_ptr) = self.new_request();

        // IoSlice and libc::iovec are guaranteed to have the same representation
        let iovec =
            unsafe { std::mem::transmute::<*const IoSlice, *const libc::iovec>(buf_vec.as_ptr()) };

        self.queue.blkioq.borrow_mut().writev(
            start,
            iovec,
            buf_vec.len() as u32,
            rs_ptr as usize,
            flags,
        );

        r
    }

    /**
     * Issue a flush.
     */
    pub fn flush<'a>(&self, flags: blkio::ReqFlags) -> Request<'a> {
        let (r, rs_ptr) = self.new_request();

        self.queue.blkioq.borrow_mut().flush(rs_ptr as usize, flags);

        r
    }

    #[cfg(feature = "x-blkio-test")]
    pub fn push_test_command(&self, cmd: blkio::TestCommand) {
        self.queue.blkioq.borrow_mut().push_test_command(cmd)
    }

    #[cfg(feature = "x-blkio-test")]
    pub fn next_debug_log_entry(&self) -> Option<blkio::DebugLogEntry> {
        self.queue.blkioq.borrow_mut().next_debug_log_entry()
    }
}

impl Queue {
    /**
     * Create a new `Queue` object, encapsulating the given blkio queue.
     */
    fn new(blkioq: &'static mut blkio::Blkioq) -> Self {
        blkioq.set_completion_fd_enabled(true);
        let cfd = blkioq.get_completion_fd().unwrap();

        Queue {
            blkioq: RefCell::new(blkioq),
            completion_fd: Cell::new(Some(AsyncFd::new(cfd).unwrap())),
            followers: RefCell::new(VecDeque::new()),
            leaderless: Cell::new(true),
        }
    }

    /**
     * Fetch completions from libblkio, and wake all requests that are done.
     *
     * This function must only be called by the "leader" (i.e. from `RequestState::poll_leader()`).
     */
    fn process_completions(&self) {
        // Safe because the array elements are still MaybeUninit<_>
        let mut completions: [std::mem::MaybeUninit<blkio::Completion>; 64] =
            unsafe { std::mem::MaybeUninit::uninit().assume_init() };

        loop {
            let completed = match self
                .blkioq
                .borrow_mut()
                .do_io(&mut completions, 0, None, None)
            {
                Ok(c) => c,
                _ => return,
            };

            for c in completions.iter_mut().take(completed) {
                // libblkio guarantees this is safe
                let c = unsafe { c.assume_init_mut() };

                let rs_ptr = c.user_data as *const RequestState;
                let rs = unsafe { Rc::from_raw(rs_ptr) };

                let ret = if c.ret < 0 { c.ret } else { 0 };
                let old_val = rs.ret.replace(ret);
                assert!(old_val == 1);

                // `process_completions()` is always called by the leader, no reason to wake it
                if !rs.leader.get() {
                    rs.wake();
                }
            }

            if completed < 64 {
                break;
            }
        }
    }

    /**
     * Try to get an `AsyncFd` for the completion FD.  This is only possible if no `AsyncFd` exists
     * for the completion FD yet, because there can be only one at a time.
     * Note that you can also register only a single waker with a given `AsyncFd`, which means that
     * at most one request can wait on the completion FD at a time.
     */
    fn claim_completion_fd(&self) -> Option<AsyncFd<i32>> {
        // Because only a single request can wait on the completion FD at a time (as described
        // above), steal the completion FD from the `Queue` and transfer ownership to the request.
        self.completion_fd.take().take()
    }

    /**
     * Release the completion FD so that other requests can create their own `AsyncFd` objects to
     * wait on it.
     */
    fn release_completion_fd(&self, async_fd: AsyncFd<i32>) {
        // Attempt to clear the event (should not block), ignore the result
        let mut buf = std::mem::MaybeUninit::<u64>::uninit();
        let _ = unsafe {
            libc::read(
                *async_fd.get_ref(),
                buf.as_mut_ptr() as *mut libc::c_void,
                std::mem::size_of::<u64>(),
            )
        };

        // Transfer ownership of the completion FD back to the `Queue`.
        self.completion_fd.replace(Some(async_fd));
    }

    fn wake_new_leader(&self) {
        let mut followers = self.followers.borrow_mut();
        while let Some(rs) = followers.pop_front() {
            if rs.ret.get() > 0 {
                rs.leader.replace(true);
                // Note that the `waker` may be `None`, in which case this will not do anything.
                // That is fine, though, because such a case will only occur for not-yet-polled
                // requests or ones that have been woken by `process_completions()`.  In both
                // cases, the request must be on track to be polled anyway.
                rs.wake();
                return;
            }
        }
        self.leaderless.replace(true);
    }
}

impl RequestState {
    /**
     * Create a new `RequestState` object for the given queue.
     */
    pub fn new(q: &Rc<Queue>, leader: bool) -> Self {
        RequestState {
            ret: Cell::new(1),
            leader: Cell::new(leader),
            waker: Cell::new(None),
            queue: Rc::clone(q),
            completion_fd_waiter: Cell::new(None),
        }
    }

    /**
     * Notify this request's waker.
     */
    fn wake(&self) -> bool {
        if self.ret.get() <= 0 {
            /* Already completed, skip */
            return false;
        }

        let waker = self.waker.take().take();
        if let Some(w) = waker {
            w.wake();
            true
        } else {
            false
        }
    }

    /**
     * Check whether this request has completed, and if so, return the result.
     */
    fn check_completion(&self) -> Option<io::Result<()>> {
        match self.ret.get() {
            0 => Some(Ok(())),
            err if err < 0 => Some(Err(io::Error::from_raw_os_error(-err))),
            _ => None,
        }
    }

    /**
     * Just check whether the request has completed, do not submit any I/O.
     */
    fn poll_follower(self: &Rc<Self>) -> Poll<io::Result<()>> {
        match self.check_completion() {
            Some(result) => Poll::Ready(result),
            None => Poll::Pending,
        }
    }

    /**
     * Fetch completions from libblkio, and then check this request for completion.  If the request
     * is still pending, have it be woken by activity on the completion FD.
     */
    fn poll_leader(self: &Rc<Self>, cx: &mut Context<'_>) -> Poll<io::Result<()>> {
        self.queue.process_completions();

        if let Some(result) = self.check_completion() {
            if let Some(cfdw) = self.completion_fd_waiter.take().take() {
                self.queue.release_completion_fd(cfdw);
            }
            return Poll::Ready(result);
        }

        let completion_fd_waiter = match self.completion_fd_waiter.take().take() {
            Some(cfdw) => cfdw,
            None => {
                // Must be claimable, because no request but the leader may do so
                self.queue.claim_completion_fd().unwrap()
            }
        };

        let polled_cfd = completion_fd_waiter.poll_read_ready(cx);

        match polled_cfd {
            // Completion FD is pending, keep the AsyncFd around and wait to be woken
            Poll::Pending => {
                self.completion_fd_waiter
                    .replace(Some(completion_fd_waiter));
                Poll::Pending
            }

            // Completion FD is readable, clear that condition and check for completions
            // once more (by repeating the poll)
            Poll::Ready(Ok(mut g)) => {
                g.clear_ready();
                self.completion_fd_waiter
                    .replace(Some(completion_fd_waiter));
                self.poll_leader(cx)
            }

            // Some error occurred, return it
            Poll::Ready(Err(e)) => {
                self.queue.release_completion_fd(completion_fd_waiter);
                Poll::Ready(Err(e))
            }
        }
    }
}

impl Future for Request<'_> {
    type Output = io::Result<()>;

    /**
     * Poll this request for progress.
     */
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        #[cfg(feature = "x-blkio-test")]
        self.rs
            .queue
            .blkioq
            .borrow_mut()
            .notify_poll(self.rs.as_ref() as *const RequestState as usize);

        if self.rs.leader.get() {
            let result = self.rs.poll_leader(cx);
            if matches!(result, Poll::Ready(_)) {
                // We are done, select a new leader (and wake it)
                self.rs.leader.replace(false);
                self.rs.queue.wake_new_leader();
            }
            result
        } else {
            let result = self.rs.poll_follower();
            if matches!(result, Poll::Pending) {
                // Figure out whether we need to set `self.rs.waker`.  If it is already set and the
                // same waker as `cx.waker()`, no need to clone the latter.
                // (If the request is done, there is no need to set a waker at all.)
                let waker = match self.rs.waker.take() {
                    Some(waker) => {
                        if waker.will_wake(cx.waker()) {
                            waker
                        } else {
                            cx.waker().clone()
                        }
                    }
                    None => cx.waker().clone(),
                };
                self.rs.waker.replace(Some(waker));
            }
            result
        }
    }
}

impl Drop for Queue {
    /**
     * Drain the followers list when the queue is destroyed.
     */
    fn drop(&mut self) {
        let mut followers = self.followers.borrow_mut();
        while followers.pop_front().is_some() {}
    }
}
